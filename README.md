# Interpol

## Prerequisites

This project has been tested with NodeJS v16.15.1 and npm 8.11.0.

## Available Scripts

In the project directory, you can run:

### `npm i`

Downloads and installs project dependencies defined in package.json file.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Runs units test.

## Docker Deployment

In the project directory, you can run:

### `docker build -f Dockerfile -t interpol_frontend .`

Creates a docker image.

### `docker run -it -p 4001:3000 interpol_frontend`

Runs the docker image.
Open [http://localhost:4001](http://localhost:4001) to view it in your browser.
