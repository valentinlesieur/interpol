/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-shadow */
import React, { useState } from "react";

// charts
import {
  ScatterChart,
  Scatter,
  CartesianGrid,
  XAxis,
  YAxis,
  Cell,
  ResponsiveContainer,
} from "recharts";

// custom libs
import InterpolSerie from "./libs/points/serie";
import "./App.css";

function App() {
  const [json, setJson] = useState("");
  const [sortedJson, setSortedJson] = useState("");
  const [x, setX] = useState(2.5);
  const [result, setResult] = useState("");
  const [serie, setSerie] = useState();
  const [chartData, setChartData] = useState();
  const [point, setPoint] = useState();

  const readJSONFile = async () => {
    const pickerOpts = {
      types: [
        {
          description: "Images",
          accept: {
            "JSON/*": [".json"],
          },
        },
      ],
      excludeAcceptAllOption: true,
      multiple: false,
    };
    try {
      const [fileHandle] = await window.showOpenFilePicker(pickerOpts);
      const file = await fileHandle.getFile();
      const json = await file.text();
      setJson(json);
    } catch (e) {
      // console.error(e);
      throw new Error("Error when reading file!");
    }
  };

  const sort = async () => {
    const serie = new InterpolSerie();
    serie.loadPointsFromPayload(json);
    await serie.sort();
    const sortedJson = serie.toJson();
    setSortedJson(sortedJson);
    setSerie(serie);
  };

  const interpolate = async () => {
    const result = serie.interpolate(x);
    const y = result.at(-1);
    const point = { x, y, color: "red" };
    setResult(result);
    setPoint(point);
  };

  const drawChart = () => {
    const chartData = serie.toArray();
    if (point) {
      const nextX = result[1].getX();
      const index = chartData.findIndex((p) => p.x === nextX);
      chartData.splice(index, 0, point);
      setChartData(chartData);
    }
  };

  const handleXChange = (event) => {
    const { value } = event.target;
    setX(value);
  };

  const handleJsonChange = (event) => {
    const { value } = event.target;
    setJson(value);
  };

  const handleResultChange = (event) => {
    const { value } = event.target;
    setResult(value);
  };

  return (
    <div className="App">
      <div className="App-left">
        <div>Interpol</div>
        {json ? (
          <div className="App-left-data">
            <div>
              <label htmlFor="original" className="labelStyle">
                Original :
              </label>
              <textarea
                id="original"
                value={json}
                onChange={handleJsonChange}
              />
              {!sortedJson && (
                <div>
                  <button type="button" onClick={sort} className="btn">
                    Sort
                  </button>
                </div>
              )}
            </div>
            {sortedJson && (
              <div>
                <label htmlFor="sorted" className="labelStyle">
                  Sorted :
                </label>
                <textarea name="sorted" id="sorted" value={sortedJson} />
              </div>
            )}
          </div>
        ) : (
          <div>
            <button type="button" onClick={readJSONFile} className="btn">
              Load JSON data
            </button>
          </div>
        )}
        {serie && (
          <div>
            <label htmlFor="x" className="labelStyle">
              Enter x :{" "}
            </label>
            <input
              name="x"
              type="number"
              min={0}
              max={6}
              value={x}
              onChange={handleXChange}
            />
            <button type="button" onClick={interpolate} className="btn">
              Interpolate
            </button>
          </div>
        )}
        {result && (
          <div>
            <input
              type="text"
              value={result}
              onChange={handleResultChange}
              className="result"
            />
            <button type="button" onClick={drawChart} className="btn">
              Draw Chart
            </button>
          </div>
        )}
      </div>
      <div>
        {chartData && point && (
          <ResponsiveContainer id="chart" width={600} height={300}>
            <ScatterChart
              width={600}
              height={300}
              data={chartData}
              margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
            >
              <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
              <XAxis type="number" dataKey="x" />
              <YAxis />
              <Scatter name="serie" dataKey="y">
                {chartData.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={entry.color ?? "blue"} />
                ))}
              </Scatter>
            </ScatterChart>
          </ResponsiveContainer>
        )}
      </div>
    </div>
  );
}

export default App;
