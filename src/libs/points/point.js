export default class InterpolPoint {
  #x;

  #y;

  constructor(x, y) {
    const me = this;
    me.#x = x;
    me.#y = y;
  }

  getX() {
    const me = this;
    return me.#x;
  }

  getY() {
    const me = this;
    return me.#y;
  }

  setX(x) {
    const me = this;
    me.#x = x;
  }

  setY(y) {
    const me = this;
    me.#y = y;
  }

  swapXY(otherPoint) {
    const me = this;
    const x = me.#x;
    const y = me.#y;
    me.setX(otherPoint.getX());
    me.setY(otherPoint.getY());
    otherPoint.setX(x);
    otherPoint.setY(y);
  }

  toString() {
    return `(${this.#x};${this.#y})`;
  }

  toJson() {
    return JSON.stringify({ x: this.#x, y: this.#y }, null, 2);
  }

  clone() {
    const otherPoint = new InterpolPoint(this.getX(), this.getY());
    return otherPoint;
  }
}
