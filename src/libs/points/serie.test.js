import "@testing-library/jest-dom";

import InterpolSerie from "./serie";

it("interpolates", () => {
  const json = `{
        "points": [
          {
            "x": 3,
            "y": 0.1
          },
          {
            "x": 0,
            "y": 0
          },
          {
            "x": 6,
            "y": -0.2
          },
          {
            "x": 1,
            "y": 0.8
          },
          {
            "x": 2,
            "y": 0.9
          },
          {
            "x": 4,
            "y": -0.7
          },
          {
            "x": 5,
            "y": -1
          }
        ]
      }`;
  const serie = new InterpolSerie();
  serie.loadPointsFromPayload(json);
  serie.sort();
  const result = serie.interpolate(2.5);
  expect(Array.isArray(result)).toBe(true);
  expect(result.length).toBe(3);
  expect(result[2]).toBe(0.5);
});
