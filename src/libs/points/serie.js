import InterpolPoint from "./point";

export default class InterpolSerie {
  #points = [];

  #length = 0;

  constructor(points = []) {
    this.setPoints(points);
  }

  loadPointsFromPayload(json) {
    const object = JSON.parse(json);
    if (!("points" in object)) {
      throw new Error("Wrong JSON payload: 'points' property is missing!");
    }
    this.setPoints(object.points);
  }

  getPoints() {
    return this.#points;
  }

  setPoints(points = []) {
    this.#points = [];
    this.#length = points.length;
    for (let i = 0; i < this.#length; ++i) {
      const { x, y } = points.at(i);
      this.#points.push(new InterpolPoint(x, y));
    }
  }

  getPointAt(index) {
    return this.#points.at(index);
  }

  interpolate(x = 0) {
    const x1 = Math.floor(x);
    const x2 = x1 + 1;
    const pos = this.#partition(x1, x2);
    const y1 = this.getPointAt(pos - 1).getY();
    const y2 = this.getPointAt(pos).getY();
    const y = InterpolSerie.cosineInterpolate(y1, y2);
    // const y = InterpolSerie.linearInterpolate(y1, y2);

    return [new InterpolPoint(x1, y1), new InterpolPoint(x2, y2), y];
  }

  static linearInterpolate(y1, y2) {
    return (y1 + y2) / 2;
  }

  static cosineInterpolate(y1, y2, t = 0.5) {
    const mu = (1 - Math.cos(t * Math.PI)) / 2;
    return y1 + (y2 - y1) * mu;
  }

  #partition(left, right) {
    let pivot = right + left;
    pivot = Math.floor(pivot / 2);
    pivot = this.#points.at(pivot).getX();

    while (left <= right) {
      while (this.getPointAt(left).getX() < pivot) {
        ++left;
      }
      while (this.getPointAt(right).getX() > pivot) {
        --right;
      }

      if (left <= right) {
        this.swapPointsAt(right, left);
        left++;
        right--;
      }
    }
    return left;
  }

  async sort(metrics = true) {
    // eslint-disable-next-line no-console
    metrics && console.time("counting-sort");
    this.#countingSort();
    // this.#mergeSort();
    // await this.#quickSort(false);
    // this.#insertionSort();
    // this.#jsSort();
    // eslint-disable-next-line no-console
    metrics && console.timeEnd("counting-sort");
  }

  #jsSort() {
    this.#points.sort((a, b) => a.getX() - b.getX());
  }

  #insertionSort() {
    const length = this.getLength();
    for (let i = 1; i < length; ++i) {
      const point = this.getPointAt(i);
      let k;
      for (k = i - 1; k >= 0; --k) {
        const otherPoint = this.getPointAt(k - 1);
        if (point.getX() < otherPoint.getX()) break;
        this.getPointAt(k).setX(this.getPointAt(k - 1).getX());
        this.getPointAt(k).setY(this.getPointAt(k - 1).getY());
      }
      this.getPointAt(k).setX(this.getPointAt(k - 1).getX());
      this.getPointAt(k).setY(this.getPointAt(k - 1).getY());
    }
  }

  #countingSort() {
    const max = Math.max(...this.getPoints().map((p) => p.getX()));
    const count = new Array(max + 1).fill(0);

    // count frequency
    const length = this.getLength();
    for (let i = 0; i < length; ++i) {
      const x = this.getPointAt(i).getX();
      count[x] += 1;
    }

    // accumulate
    for (let i = 0; i < length - 1; ++i) count[i + 1] += count[i];

    // sort
    const points = new Array(length);
    for (let i = length - 1; i >= 0; --i) {
      const point = this.getPointAt(i);
      const x = point.getX();
      count[x] -= 1;
      points[count[x]] = point;
      // console.log(points);
    }
    this.#points = points;
  }

  async #quickSort(sync, left, right) {
    // console.log(this.toString()); //uncomment to follow sort steps
    sync ??= false;
    left ??= 0;
    right ??= this.getLength() - 1;

    if (left < right) {
      const limit = this.#partition(left, right);

      if (sync) {
        await this.#quickSort(sync, left, limit - 1);
        await this.#quickSort(sync, limit, right);
      } // parallelism
      else {
        const promises = [];
        promises.push(this.#quickSort(sync, left, limit - 1));
        promises.push(this.#quickSort(sync, limit, right));
        await Promise.all(promises);
      }
    }
  }

  #mergeSort(
    points // dichotomic sort
  ) {
    points ??= this.#points;
    // console.log("before merge", points);

    const { length } = points;
    if (length <= 1) return points;

    const { left, right } = this.#divide(points);
    points = InterpolSerie.#conquer(left, right);
    // console.log("after merge", points);
    if (points.length === this.getLength()) this.#points = points;
    return points;
  }

  #divide(points) {
    const { length } = points;
    const middle = length / 2;
    let left = [];
    let right = [];

    for (let i = 0; i < length; ++i) {
      if (i < middle) left.push(points[i]);
      else right.push(points[i]);
    }
    left = this.#mergeSort(left);
    right = this.#mergeSort(right);
    return { left, right };
  }

  static #conquer(left = [], right = []) {
    const result = [];
    while (left.length > 0 && right.length > 0) {
      if (left[0].getX() <= right[0].getX()) result.push(left.shift());
      else result.push(right.shift());
    }
    return [...result, ...left, ...right];
  }

  getLength() {
    return this.#length;
  }

  swapPointsAt(index1, index2) {
    this.#points.at(index1).swapXY(this.#points.at(index2));
  }

  toString() {
    const array = [];
    const length = this.getLength();
    for (let i = 0; i < length; ++i) {
      array.push(this.getPointAt(i).toString());
    }
    return array.join(", ");
  }

  toArray() {
    const array = [];
    const length = this.getLength();
    for (let i = 0; i < length; ++i) {
      const point = this.getPointAt(i);
      array.push({ x: point.getX(), y: point.getY() });
    }
    return array;
  }

  toJson() {
    const json = { points: [] };
    const length = this.getLength();
    for (let i = 0; i < length; ++i) {
      const p = this.getPointAt(i);
      json.points.push({ x: p.getX(), y: p.getY() });
    }

    return JSON.stringify(json, null, 2);
  }

  clone() {
    const otherSerie = new InterpolSerie();
    const points = [];
    const length = this.getLength();
    for (let i = 0; i < length; ++i) {
      points.push(this.getPointAt(i).clone());
    }
    otherSerie.#points = points;
    return otherSerie;
  }
}
